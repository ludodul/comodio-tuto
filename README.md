# Ceci est un tutoriel pour l'utilisation de Comodio en tant qu'enseignant.

## Principe

Comodio sert à envoyer des commentaires audios aux élèves qu'ils peuvent consulter facilement sur leur mobile en scannant un QR code.

## Création de compte et connexion

Pour pouvoir utiliser l'application __comodio__ il vous faut un compte dont vous devez demander la création à ludodul2002@gmail.com.

Une fois vos identifiants obtenus, rendez-vous sur https://comodio.fr

<img src="images/login.png">

Vous devriez arriver sur cette page avec votre identifiant

<img src="images/bienvenue.png">

## Ajouter/supprimer un groupe

Pour ajouter ou supprimer un groupe il faut aller dans le menu __Actions__ et sélectionner __Ajouter Groupe__

<img src="images/aj_groupe.png">

Vous obtiendrez un écran similaire à celui-ci

<img src="images/aj_groupe2.png">

Il suffit alors d'entrer un nom de groupe dans la barre de saisie en dessous de la liste des groupes existants et de cliquer sur __Ajouter__. L'icone de la poubelle à droite vous permet d'effacer un groupe. Soyez précis dans le nom du groupe pour éviter les ambiguïtés. Par exemple un groupe qui s'appelerait Maths_1ere ne serait pas assez précis car il y a plusieurs groupes de maths en 1ere.

## Ajouter/supprimer un élève

L'élève est au centre de l'application, il faut que l'utilisation soit la plus simple possible pour lui. 

Pour ajouter un élève il y a deux possibilités:
 * Si vous voulez entrer vous-même les élèves il faut cliquer sur __Ajouter Elève__ dans le menu __Actions__.

 <img src="images/aj_groupe.png">

Apparaîtra alors la liste de tous les élèves déja inscrits par ordre alphabétique

<img src="images/aj_eleve.png">

Vous pouvez ajouter ou supprimer des élèves. Attention aux doublons...

  * Deuxième possibilité, les élèves peuvent s'inscrire eux-même en leur donnant l'adresse suivant https://comodio.fr/contact

<img src="images/contact.png">

Il n'ont qu'à remplir le formulaire, cliquer sur envoyer, et enregistrer le qr code qui leur sera donné. Ils n'auront pas d'autre possibilité pour obtenir eux-même leur code. S'ils ne le font pas, il faudra qu'ils vous demandent de leur renvoyer leur code auquel vous aurez accès.

En cette période de confinement il est plus simple qu'ils procèdent ainsi, sinon il faudra leur envoyer leur code un par un ce qui n'est pas très pratique...
La partie concernant les codes est un peu plus bas.

## Mettre des élèves dans des groupes

Maintenant que les groupes et les élèves sont créés, il faut mettre les élèves dans le groupe que vous avez créé. Un élève peut appartenir à plusieurs groupes, ce n'est pas génant. Dans le menu __Groupes__ sélectionner le groupe que vous avez créé. 

<img src="images/select_grp.png">

Vous obtenez un groupe qui est normalement vide la première fois que vous y accédez

<img src="images/grp_vide.png">

Il faut alors rechercher les élèves à ajouter dans la barre de recharche puis cliquer sur __ajouter élève__ pour ajouter un élève au groupe

<img src="images/groupe_rech.png">

On peut biensûr enlever un élève du groupe une fois qu'il est dedans.

Sur la partie droite, on peut ajouter des noms de devoirs ou bien de messages que l'on veut envoyer aux élèves.

<img src="images/groupe_comp.png">

## message perso / message global

Le but de cette application est d'envoyer des messages audios aux élèves afin d'éviter d'avoir à écrire des mails à rallonge, et on peut ainsi entrer plus dans le détail sur certains travaux ce qui peut être un peu lours à l'écrit.

Il est donc possible d'envoyer un message personalisé à chaque élève inscrit sur __comodio__ à partir du moment où il est dans un groupe qui comporte au moins un devoir.

Il y a deux façons d'envoyer un message. Soit un message personalisé à un élève, soit un message global sur un devoir à tous les élèves du groupe.

Pour une message personnalisé, il faut aller dans le menu __Message perso__

<img src="images/mess_pers_menu.png">

Puis sélectionner un groupe

<img src="images/groupe_code_test2.png">

Chaque élève a deux codes, un qui est le sien, et l'autre qui est celui du prof. le __Code eleve__ est le code qu'a obtenu l'élève lors de son inscription, et c'est celui qu'il faudra lui redonner en cas de perte ou lui donner si vous l'inscrivez vous-même.(il est également accessible depuis la page __Ajouter Elève__)

Le __Code prof__ est le code qui vous permettra d'envoyer des messages audio à cet élève, c'est donc celui que vous utiliserez, et que vous ne devez pas communiquer à l'élève, ni à personne d'autre. En gros chaque élève possède un code et chaque prof possède un code par élève. Cette liste est téléchargeable au format ODT directement en cliquant sur le bouton __Télécharger__ en haut à droite.

Pour ceux qui n'ont pas de mobile, il est possible de cliquer sur le lien __commentaire__ pour envoyer directement vos commentaires à vos élèves. Dans ce cas les __Code prof__ ne vous servent pas.

En scannant le __Code prof__ ou en cliquant sur le lien commentaire, on obtient la page suivante:


<img src="images/eleve_test1.png">

En cliquant sur choisir un fichier, soit cela ouvre votre microphone si vous êtes sur mobile et il n'y a plus qu'à enregistrer le message à envoyer, soit cela ouvre l'explorateur, et il suffira de choisir le fichier audio à envoyer que vous aurez préalablement enregistré, il faudra donc un micro et savoir enregistrer un message sur votre ordinateur. Il suffit ensuite d'appuyer sur __Envoyer__ de patienter un peu lors de l'envoi du fichier.

<img src="images/eleve_test12.png">

Il est biensur possible de supprimer un message audio en cliquant sur la poubelle.

Pour envoyer un __Message global__ Il suffit de cliquer sur le menu, vous obtiendrez un QR code et un lien, QR code qui n'est utile que si vous avez un mobile, sinon le lien fera l'affaire. (Les deux pointent sur la même page)

<img src="images/message_global.png">

On retrouve tous les devoirs que l'on a créé associés aux groupes dans lequel ils sont.
A noter que envoyer un __Message global__ sur un devoir sur lequel vous avez déjà laissé des __Méssage perso__, écrasera ces derniers. Donc si sur un devoir vous voulez créer un message global et un message perso par élève, il faudra créer 2 devoirs, un consacré au message global et l'autre au message perso.